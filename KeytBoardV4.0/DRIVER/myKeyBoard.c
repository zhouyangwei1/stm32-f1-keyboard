#include "main.h"
#include "encode.h"
#include "myKeyBoard.h"
#include <string.h>

#define myKeyBoard_row0_GPIOPort		GPIOA
#define myKeyBoard_row0_GPIOPin		GPIO_Pin_15
#define myKeyBoard_row1_3_GPIOPort		GPIOB
#define myKeyBoard_row1_GPIOPin		GPIO_Pin_3
#define myKeyBoard_row2_GPIOPin		GPIO_Pin_4
#define myKeyBoard_row3_GPIOPin		GPIO_Pin_5


#define myKeyBoard_col3_0_GPIOPort	GPIOB
#define myKeyBoard_col0_GPIOPort	GPIOB
#define myKeyBoard_col1_GPIOPort	GPIOB
#define myKeyBoard_col2_GPIOPort	GPIOB
#define myKeyBoard_col3_GPIOPort	GPIOB
#define myKeyBoard_col0_GPIOPin		GPIO_Pin_12
#define myKeyBoard_col1_GPIOPin		GPIO_Pin_2
#define myKeyBoard_col2_GPIOPin		GPIO_Pin_1
#define myKeyBoard_col3_GPIOPin		GPIO_Pin_0


#define myKeyBoard_ControlKey 0x00
#define myKeyBoard_SpecialKey 0x00

#define xxCK	myKeyBoard_ControlKey 
#define xxSK	myKeyBoard_SpecialKey 

const char * myKeyBoard_KeyMap_Name1[6][16] = 
{
	"Esc","F1","F2","F3","F4","F5","F6","F7","F8","F9","F10","F11","F12","PrtSc","ScrollLock","PauseBreak",
	"~","!","@","#","$","%","^","&","*","(",")","-","+","BackSpace","Insert","Home",
	"Tab","Q","W","E","R","T","Y","U","I","O","P","{","}","|","Delete","End",
	"CapsLock","A","S","D","F","G","H","J","K","L",";","'","Enter","","","PageUp",
	"LShift","Z","X","C","V","B","N","M",",",".","/","RShift","Up","","","PageDown",
	"LCtrl","Win","LAlt","Space","RAlt","FN","Menu","RCtrl","Left","Down","Right","","","","",""
};

  
const unsigned char myKeyBoard_KeyMap_ATValue1[6][16] = 
{
	0x29,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,
	0x35,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x2D,0x2E,0x2A,0x49,0x4A,
	0x2B,0x14,0x1A,0x08,0x15,0x17,0x1C,0x18,0x0C,0x12,0x13,0x2F,0x30,0x31,0x4C,0x4D,
	0x39,0x04,0x16,0x07,0x09,0x0A,0x0B,0x0D,0x0E,0x0F,0x33,0x34,0x28,0x00,0x00,0x4B,
	xxCK,0x1D,0x1B,0x06,0x19,0x05,0x11,0x10,0x36,0x37,0x38,xxCK,0x52,0x00,0x00,0x4E,
	xxCK,xxCK,xxCK,0x2C,xxCK,xxSK,xxSK,xxCK,0x50,0x51,0x4F,0x00,0x00,0x00,0x00,0x00,
};

const char * myKeyBoard_KeyMap_Name[4][4] = 
{
	"Esc"			,"Win"	,"LAlt"		,"Tab",
	"LCtrl"		,"Z"		,"Y"			,"BackSpace",
	"CapsLock","Up"		,"LShift"	,"Enter"
	"Left"		,"Down"	,"Right"	,"Space",
};			

const unsigned char myKeyBoard_KeyMap_ATValue[4][4] = 
{	
	0x29,xxCK,xxCK,0x2B,
	xxCK,0x1D,0x1C,0x2A,
	0x39,0x52,xxCK,0x28,
	0x50,0x51,0x4F,0x2C,
};

//const unsigned char myKeyBoard_KeyMap_ATValue[6][16] = 
//{
//	0x29,0x3A,0x3B,0x3C,0x3D,0x3E,0x3F,0x40,0x41,0x42,0x43,0x44,0x45,0x46,0x47,0x48,
//	0x35,0x1E,0x1F,0x20,0x21,0x22,0x23,0x24,0x25,0x26,0x27,0x2D,0x2E,0x2A,0x49,0x4A,
//	0x2B,0x14,0x1A,0x08,0x15,0x17,0x1C,0x18,0x0C,0x12,0x13,0x2F,0x30,0x31,0x4C,0x4D,
//	0x39,0x04,0x16,0x07,0x09,0x0A,0x0B,0x0D,0x0E,0x0F,0x33,0x34,0x28,0x00,0x00,0x4B,
//	0xE1,0x1D,0x1B,0x06,0x19,0x05,0x11,0x10,0x36,0x37,0x38,0xE5,0x52,0x00,0x00,0x4E,
//	0xE0,0xE3,0xE2,0x2C,0xE6,0x00,0x76,0xE4,0x50,0x51,0x4F,0x00,0x00,0x00,0x00,0x00,
//};

const uint16_t key_Col_Pin[4] = 
{
	myKeyBoard_col0_GPIOPin,
	myKeyBoard_col1_GPIOPin,
	myKeyBoard_col2_GPIOPin,	
	myKeyBoard_col3_GPIOPin,	
};

typedef struct 
{
	unsigned char ATKeyData;
	unsigned char rowN;
	unsigned char colN;
}ATKeyDataStructureTypedef;

//普通按键暂存数组
ATKeyDataStructureTypedef ATKeyData[6];
unsigned char ATKeyControlByte5 = 0x00;//状态控制键字节 如Shift Ctrl键等
//4行x4列键盘 状态数组
KeyState_enumTypedef myKeyBoard_KeyState[4][4] = {KEYUNPRESSED};//当前按键状态
KeyState_enumTypedef myKeyBoard_KeyState_Ex[4][4] = {KEYUNPRESSED};//上次按键状态

u8 LOGO_LED_Status = 0;

unsigned char myKeyBoard_KeyStateChangedFlag = 0;//键盘状态改变标志
unsigned char myKeyBoard_KeyIsControl = 1;//键盘控制按键标志
unsigned char g_myKeyBoard_DataWaitForUploadFlag = 0;//数据等待上传标志


void myKeyBoard_ControlKeyProcess(u8 rowi, u8 colj);
void myKeyBoard_KeyScan_rowScan(GPIO_TypeDef * 		col_GPIOPort, 
								uint8_t 			col_GPIOPin_Index,
								const uint16_t *	p_col_GPIOPin);





//键盘更新
char Comsumer_Mode = 0;
void myKeyBoard_ScanKeyAndUpdataATBuffer()
{
	myKeyBoard_KeyScan();//物理层键盘状态扫描，按键是否被按下 ， 
	myKeyBoard_JudgeKeyStateChange();//判断键盘状态是否有变化，有则置位myKeyBoard_KeyStateChangedFlag标志
	

	
	if(myKeyBoard_KeyStateChangedFlag)//判断是否有变化
	{
		myKeyBoard_KeyStateChangedFlag = 0;
		myKeyBoard_UpdataATDataPack();//键盘AT数据包更新
		g_myKeyBoard_DataWaitForUploadFlag = 1;//置位数据等待上传标志
	}
	else{
		
	}
}

/*
*/
    


//定义 FN组合键的枚举，方便下面键值处理
enum{
	none = 0,
	Mute = 1,
	VolumeUp = 2,
	VolumeDown = 3,
	PlayORPause = 4,
	Stop = 5
}	specialKeyenum = none;

void ConsumerKeyProcess(void)//处理FN组合功能键，并发送给BTK05
{
	BTK05_ConsumerPack[5] = 0x00;//清零
	switch(specialKeyenum)
	{
		case Mute:
			//specialATKeyValueTemp = 0x7F;
			BTK05_ConsumerPack[5] |= (0x01 << 0); 
			break;
		case VolumeUp:
			BTK05_ConsumerPack[5] |= (0x01 << 2);
			break;
		case VolumeDown:
			BTK05_ConsumerPack[5] |= (0x01 << 1);
			break;
		case PlayORPause:
			BTK05_ConsumerPack[5] |= (0x01 << 3);
			break;
		case Stop:
			BTK05_ConsumerPack[5] |= (0x01 << 4);
			break;
		default:
			break;
	}

	BTK05_ConsumerPack[5] = 0x00;//清零

}

void myKeyBoard_UpdataATDataPack()
{
	u8 i,j,k;
	u8 AlreadeyExistflag = 0;
	ATKeyControlByte5 = 0x00;//清楚控制按键键值

		
	for(i = 0; i < 6 ; i++)//判断上次按下的按键现在是否还被按下,若否则将ATKeyData的数据清零
	{
		if(ATKeyData[i].ATKeyData != 0x00)
		{
			if(myKeyBoard_KeyState[ATKeyData[i].rowN][ATKeyData[i].colN] == KEYUNPRESSED)
			{
				ATKeyData[i].ATKeyData = 0x00;
			}
		}
	}
	
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			if(myKeyBoard_KeyState[i][j] == KEYPRESSED)//如果按键被按下则处理键盘数据包
			{
				myKeyBoard_ControlKeyProcess(i,j);//控制按键处理
				if(myKeyBoard_KeyIsControl == 0)
				{
					for(k = 0; k < 6; k++)//判断ATKeyData数据包里是否已经有该键的值了，即是否之前已经被按下了
					{
						if(ATKeyData[k].ATKeyData == myKeyBoard_KeyMap_ATValue[i][j])
						{
							AlreadeyExistflag = 1;
							break;
						}
					}
					if(AlreadeyExistflag == 0)//如果ATKeyData数据包没有该键值则放入
					{
						//循环	只要ATKeyData数据包有空位则插入该键值，并且break。
						//如果六次循环完毕也没插就无效，因为一次只能按下六个按键
						for(k = 0; k < 6; k++)
						{
							if(ATKeyData[k].ATKeyData == 0x00)//将按键AT值放入ATKeyData  Buffer
							{
								ATKeyData[k].ATKeyData = myKeyBoard_KeyMap_ATValue[i][j];
								ATKeyData[k].rowN = i;
								ATKeyData[k].colN = j;
								break;
							}
						}
					}
					else {
						AlreadeyExistflag = 0;}
					myKeyBoard_KeyIsControl = 1;
				}
			}
			
		}
	}
	
	//按键键值判断完毕后，将键盘数据包更新
	BTK05_ATKeyDataPack[4] = ATKeyControlByte5;	//更新控制按键字节
	

	for(k = 0; k < 6; k++)
	{
		BTK05_ATKeyDataPack[6 + k] = ATKeyData[k].ATKeyData;//更新普通按键
	}
}


void myKeyBoard_ControlKeyProcess(u8 rowi, u8 colj)
{
	if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"LShift") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 1);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"RShift") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 5);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"LCtrl") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 0);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"RCtrl") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 4);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"LAlt") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 2);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"RAlt") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 6);
	}
	else if(strcmp(myKeyBoard_KeyMap_Name[rowi][colj],"Win") == 0)
	{
		ATKeyControlByte5 = ATKeyControlByte5 | (0x01 << 3);
	}
	else//如果以上都没有判别，则是普通按键，复位控制按键标志
	{
		myKeyBoard_KeyIsControl = 0;
	}
}


void myKeyBoard_JudgeKeyStateChange()
{
	u8 i,j;
	//检查键盘状态是否改变
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			if(myKeyBoard_KeyState_Ex[i][j] != myKeyBoard_KeyState[i][j])
			{
				myKeyBoard_KeyStateChangedFlag = 1;
				break;
			}
			
		}
		if(myKeyBoard_KeyStateChangedFlag)
			break;
	}
	
	//将当前的键盘状态传递给上次
	for(i = 0; i < 4; i++)
	{
		for(j = 0; j < 4; j++)
		{
			myKeyBoard_KeyState_Ex[i][j] = myKeyBoard_KeyState[i][j];//将当前的键盘状态传递给上次
		}
	}
	
}


#define	C1_OUT PBout(12)
#define	C2_OUT PBout(2)
#define	C3_OUT PBout(1)
#define	C4_OUT PBout(0)
#define R1_IN	 PAin(15)
#define R2_IN	 PBin(3)
#define R3_IN	 PBin(4)
#define R4_IN	 PBin(5)
void zhouKeyScan(void){
	char change_flag = 0;
	C1_OUT = 1;
	DelayUs(10);
	if(R1_IN)	{myKeyBoard_KeyState[0][0] = KEYPRESSED;	change_flag = 1;}
	else	myKeyBoard_KeyState[0][0]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R2_IN)	{myKeyBoard_KeyState[0][1] = KEYPRESSED;	change_flag = 1;}
	else	myKeyBoard_KeyState[0][1]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R3_IN)	{myKeyBoard_KeyState[0][2] = KEYPRESSED;	change_flag = 1;}
	else	myKeyBoard_KeyState[0][2]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R4_IN)	{myKeyBoard_KeyState[0][3] = KEYPRESSED;	change_flag = 1;}
	else	myKeyBoard_KeyState[0][3]	=	KEYUNPRESSED;
	C1_OUT = 0;
	DelayUs(10);
	
	C2_OUT = 1;
	DelayUs(5);
	if(R1_IN)	{myKeyBoard_KeyState[1][0] = KEYPRESSED;	change_flag = 1;}
	else	myKeyBoard_KeyState[1][0]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R2_IN)	{myKeyBoard_KeyState[1][1] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[1][1]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R3_IN)	{myKeyBoard_KeyState[1][2] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[1][2]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R4_IN)	{myKeyBoard_KeyState[1][3] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[1][3]	=	KEYUNPRESSED;
	C2_OUT = 0;
	DelayUs(10);
		
	
	C3_OUT = 1;
	DelayUs(5);
	if(R1_IN)	{myKeyBoard_KeyState[2][0] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[2][0]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R2_IN)	{myKeyBoard_KeyState[2][1] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[2][1]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R3_IN)	{myKeyBoard_KeyState[2][2] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[2][2]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R4_IN)	{myKeyBoard_KeyState[2][3] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[2][3]	=	KEYUNPRESSED;
	C3_OUT = 0;
	DelayUs(10);
		
	
	C4_OUT = 1;
	DelayUs(5);
	if(R1_IN)	{myKeyBoard_KeyState[3][0] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[3][0]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R2_IN)	{myKeyBoard_KeyState[3][1] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[3][1]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R3_IN)	{myKeyBoard_KeyState[3][2] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[3][2]	=	KEYUNPRESSED;
	DelayUs(10);
	if(R4_IN)	{myKeyBoard_KeyState[3][3] = KEYPRESSED; change_flag = 1;}
	else	myKeyBoard_KeyState[3][3]	=	KEYUNPRESSED; 
	C4_OUT = 0;
	
	
	if(change_flag)	myKeyBoard_KeyStateChangedFlag = 1;
	else myKeyBoard_KeyStateChangedFlag = 0;
}


void myKeyBoard_KeyScan()
{
	u8 i;
	for(i = 0 ; i < 4 ;i ++)
	{
		myKeyBoard_KeyScan_rowScan(myKeyBoard_col3_0_GPIOPort, i, key_Col_Pin);
		//DelayUs(6);
	}

}


void myKeyBoard_KeyScan_rowScan(GPIO_TypeDef * 		col_GPIOPort, 
								uint8_t 			col_GPIOPin_Index,
								const uint16_t *	p_col_GPIOPin)
{
	GPIO_SetBits(col_GPIOPort, *(p_col_GPIOPin + col_GPIOPin_Index));
	DelayUs(2);
	if(GPIO_ReadInputDataBit(myKeyBoard_row0_GPIOPort, myKeyBoard_row0_GPIOPin)) 
		{myKeyBoard_KeyState[0][col_GPIOPin_Index] = KEYPRESSED;}
	else{myKeyBoard_KeyState[0][col_GPIOPin_Index] = KEYUNPRESSED;}
	DelayUs(50);
	if(GPIO_ReadInputDataBit(myKeyBoard_row1_3_GPIOPort, myKeyBoard_row1_GPIOPin)) 
		{myKeyBoard_KeyState[1][col_GPIOPin_Index] = KEYPRESSED;}
	else{myKeyBoard_KeyState[1][col_GPIOPin_Index] = KEYUNPRESSED;}
	DelayUs(50);
	if(GPIO_ReadInputDataBit(myKeyBoard_row1_3_GPIOPort, myKeyBoard_row2_GPIOPin)) 
		{myKeyBoard_KeyState[2][col_GPIOPin_Index] = KEYPRESSED;}
	else{myKeyBoard_KeyState[2][col_GPIOPin_Index] = KEYUNPRESSED;}
	DelayUs(50);
	if(GPIO_ReadInputDataBit(myKeyBoard_row1_3_GPIOPort, myKeyBoard_row3_GPIOPin)) 
		{myKeyBoard_KeyState[3][col_GPIOPin_Index] = KEYPRESSED;}
	else{myKeyBoard_KeyState[3][col_GPIOPin_Index] = KEYUNPRESSED;}

	
	GPIO_ResetBits(col_GPIOPort, *(p_col_GPIOPin + col_GPIOPin_Index));
	//DelayUs(10);
}

unsigned char myKeyBoard_JudgeKeyPressWithName(const char* keyName)
{
	
	unsigned char i,j;
	for(i = 0; i < 4; i++ )
	{
		for(j = 0; j < 4; j++)
		{
			if(strcmp(myKeyBoard_KeyMap_Name[i][j], keyName) == 0)
			{
				if(myKeyBoard_KeyState[i][j] == KEYUNPRESSED)
				{
					return 0;
				}
				else
				{
					myKeyBoard_KeyState[i][j] = KEYUNPRESSED;
					return 1;
				}
			}
				
		}
		
	}
	
	return 0;
}

//初始化键盘IO
void myKeyBoard_GPIO_Init()
{
	
	GPIO_InitTypeDef GPIOType;
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	
	//初始化 row0-row3 GPIO
	GPIOType.GPIO_Pin = myKeyBoard_row0_GPIOPin;
	
	GPIOType.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOType.GPIO_Mode = GPIO_Mode_IPD; //下拉输入
	GPIO_Init(myKeyBoard_row0_GPIOPort , &GPIOType);
	
	GPIOType.GPIO_Pin = myKeyBoard_row1_GPIOPin | 
						myKeyBoard_row2_GPIOPin | 
						myKeyBoard_row3_GPIOPin ;
	GPIOType.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOType.GPIO_Mode = GPIO_Mode_IPD; //下拉输入
	GPIO_Init(myKeyBoard_row1_3_GPIOPort , &GPIOType);
	
	
	//初始化 col0-col3 GPIO
	GPIOType.GPIO_Pin = myKeyBoard_col0_GPIOPin | 
						myKeyBoard_col1_GPIOPin | 
						myKeyBoard_col2_GPIOPin | 
						myKeyBoard_col3_GPIOPin ;

	GPIOType.GPIO_Speed = GPIO_Speed_50MHz;
	GPIOType.GPIO_Mode = GPIO_Mode_Out_PP;		//推挽输出
	GPIO_Init(myKeyBoard_col3_0_GPIOPort , &GPIOType);
	

}





uint8_t USER_USB_HIDEP1_TX_LEN = 0;
uint8_t USER_USB_HIDEP1_RX_LEN = 0;
uint8_t USER_USB_HIDEP1_TX_Buf[HIDEP1_REPORT_COUNT] ={0};
uint8_t USER_USB_HIDEP1_RX_Buf[HIDEP1_REPORT_COUNT] ={0};
 
uint8_t USER_USB_HIDEP2_TX_LEN = 0;
uint8_t USER_USB_HIDEP2_RX_LEN = 0;
uint8_t USER_USB_HIDEP2_TX_Buf[HIDEP2_REPORT_COUNT] ={0};
uint8_t USER_USB_HIDEP2_RX_Buf[HIDEP2_REPORT_COUNT] ={0};
 
 
int USBHID1_SendData(uint8_t *data, int dataSize)		 
{
	uint16_t tempSize = 0;
	uint16_t sendSize = 0;
 
	if(bDeviceState != CONFIGURED)
		return -1;
 
	if(dataSize == 0)
		return -2;
 
   	do{
   		memset(USER_USB_HIDEP1_TX_Buf, 0, HIDEP1_REPORT_COUNT);
 
		if(dataSize>HIDEP1_REPORT_COUNT)
			tempSize = HIDEP1_REPORT_COUNT;
		else
			tempSize = dataSize;
 
		memcpy(USER_USB_HIDEP1_TX_Buf, (uint8_t*)(data + sendSize), tempSize);
 
		USER_USB_HIDEP1_TX_LEN = tempSize;
		dataSize -= tempSize;
		sendSize += tempSize;
 
		USB_SIL_Write(ENDP1, USER_USB_HIDEP1_TX_Buf, HIDEP1_REPORT_COUNT); 
		SetEPTxValid(ENDP1);
	}while(dataSize>0);
 
	//数据发完之后清零
	DelayMs(20);
	memset(USER_USB_HIDEP1_TX_Buf, 0, HIDEP1_REPORT_COUNT);
	USB_SIL_Write(ENDP1, USER_USB_HIDEP1_TX_Buf, HIDEP1_REPORT_COUNT); 
	SetEPTxValid(ENDP1);
	return sendSize;  
}
 
int USBHID2_SendData(uint8_t *data, int dataSize)		 
{
	uint16_t tempSize = 0;
	uint16_t sendSize = 0;
 
	if(bDeviceState != CONFIGURED)
		return -1;
 
	if(dataSize == 0)
		return -2;
 
   	do{			
   		memset(USER_USB_HIDEP2_TX_Buf, 0, HIDEP2_REPORT_COUNT);
 
		if(dataSize>HIDEP2_REPORT_COUNT)
			tempSize = HIDEP2_REPORT_COUNT;
		else
			tempSize = dataSize;
 
		memcpy(USER_USB_HIDEP2_TX_Buf, (uint8_t*)(data + sendSize), tempSize);
 
		USER_USB_HIDEP2_TX_LEN = tempSize;
		dataSize -= tempSize;
		sendSize += tempSize;
 
		USB_SIL_Write(ENDP2, USER_USB_HIDEP2_TX_Buf, HIDEP2_REPORT_COUNT); 
		SetEPTxValid(ENDP2);
	}while(dataSize>0);
 
	return sendSize;  
}


