#include "main.h"
#include "timer.h"
#include "oled.h"

volatile u8 RGB_SET_FLAG = 0;
#define VOL_NULL	0x00
#define VOL_DOWN	0x02
#define VOL_UP		0x01	
#define VOL_PP		0x04	// 切换静音、播放


void TIM4_Init(u16 arr,u16 psc)
{  
    TIM_TimeBaseInitTypeDef TIM_TimeBaseStructure;

      //使能定时器TIM2时钟，注意TIM4时钟为APB1，而非APB2
    RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
                                                                          
    TIM_TimeBaseStructure.TIM_Period = arr-1;//自动重装值
    TIM_TimeBaseStructure.TIM_Prescaler =psc-1; //时钟预分频数
    TIM_TimeBaseStructure.TIM_ClockDivision = 0;
    TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;//TIM向上计数模式
    TIM_TimeBaseInit(TIM4, &TIM_TimeBaseStructure); //初始化TIM2
     
   			
		NVIC_Config(2);
	
	  TIM_ITConfig(TIM4, TIM_IT_Update, ENABLE);
	  TIM_ClearFlag(TIM4, TIM_FLAG_Update);
	  TIM_ARRPreloadConfig(TIM4, ENABLE); //使能TIMx在ARR上的预装载寄存器
    TIM_Cmd(TIM4, ENABLE);//使能定时器TIM2，准备工作 
			
}


//void TIM3_Init(u16 arr,u16 psc)
//{  
//	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;

//	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE); 
// 	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA , ENABLE);  //使能GPIO外设时钟使能
//	                                                                     	

//	                                                                                
//	TIM_TimeBaseStructure.TIM_Period = arr-1; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
//	TIM_TimeBaseStructure.TIM_Prescaler = psc-1; //设置用来作为TIMx时钟频率除数的预分频值  
//	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
//	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
//	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure); //根据TIM_TimeBaseInitStruct中指定的参数初始化TIMx的时间基数单位

//  NVIC_Config(5);
// 
//  TIM_ClearFlag(TIM3, TIM_FLAG_Update);
//  TIM_ITConfig(TIM3, TIM_IT_Update, ENABLE);
//	TIM_ARRPreloadConfig(TIM3, ENABLE); //使能TIM3在ARR上的预装载寄存器
//	TIM_Cmd(TIM3, DISABLE);  //使能TIM3
//	
//}



void TIM2_PWM_set_ms(u16 ch1,u16 ch2)
{
	TIM_SetCompare1(TIM2,ch1);//
  TIM_SetCompare2(TIM2,ch2);//

}


struct LIGHT_SET rgb_set(char col,struct LIGHT_SET light_set){
	
	int enc_temp = get_enc_cnt_inc(3);
	
	if(enc_temp!=0)			RGB_SET_TIMEOFF_CNT = 0;	// 清空配置超时标志位;
	
	light_set = my_2812_set_dif(col,enc_temp,light_set);
	
	return	light_set;
	
}
	


// 定时器3中断 
void TIM3_IRQHandler(void)
{ 
	
	if(TIM_GetITStatus(TIM3, TIM_IT_Update) != RESET)//  溢出中断
	{
		
		
	}	
  TIM_ClearFlag(TIM3, TIM_FLAG_Update); 	
}

u8	vol_paus_last = 0;
void vol_ctl(){
	int temp = get_enc_cnt_inc(2);
	int temp_rgb	=	TIM3->CNT;
	
	if(temp!=0){
		//  ##########  唤醒  #########
		RGB_SET_FLAG = BREATH;
		g_myKeyBoard_DataWaitForUploadFlag	=	1;
		sleepTime1SCounter = 0;		// 清空计数
		//  ##########################
	}
	
	// ###########  音量调整  ##############
	if(temp>0)
		VLOUME_BUF[0]=VOL_UP;
	else if(temp<0)
		VLOUME_BUF[0]=VOL_DOWN;
	else
		VLOUME_BUF[0]=VOL_NULL;
	
	Volume_Send(VLOUME_BUF,1);
	
	if(VOL_PAUS_FLAG != vol_paus_last){
		if(VOL_PAUS_FLAG == 1){
			VLOUME_BUF[0]=VOL_PP;
		}
		else{
			VLOUME_BUF[0] = VOL_UP;
		}
		Volume_Send(VLOUME_BUF,1);
		VLOUME_BUF[0]=VOL_NULL;
		DelayMs(40);
		Volume_Send(VLOUME_BUF,1);
	}
	
	vol_paus_last	= VOL_PAUS_FLAG;
	
	// ###########  亮度调整  ##############
	if(temp_rgb!=0 && RGB_SET_FLAG == BREATH ){
		//my_2812_set_dif(MAX,temp_rgb,LIGHT_SET1);
		//rgb_set(MAX,LIGHT_SET1);
		RGB_SET_FLAG	=	MAX;
	}
	
}

unsigned short sleepTime1SCounter = 0;
unsigned char sleepTime50MsCounter = 0;
u8	RGB_SET_TIMEOFF_CNT	=	0;
////定时器中断服务程序
volatile u8	OLED_PIC_LFAG = 0;
void TIM4_IRQHandler(void)   //TIM中断
{
	
	if (TIM_GetITStatus(TIM4, TIM_IT_Update) != RESET)  //检查TIM更新中断发生与否
		{
			TIM_ClearITPendingBit(TIM4, TIM_IT_Update  );  //清除TIMx更新中断标志 
			
			
			
			//  ########### 计数器 ############	
			sleepTime50MsCounter++;
			//  ############# IDOL #############
			//	1S任务
			if(sleepTime50MsCounter >= 20){
				sleepTime1SCounter++;
				sleepTime50MsCounter = 0;  
			}
			//  ###############################
						
			
			//  ############## VOLUME #############
			// 0.05s任务
			vol_ctl();
						
			//  ############# 休眠  ##############
			if(system_Status	== 0)	return;
			//  ##################################
			
			//  ############## RGB #############
			//	0.15S任务
			if(sleepTime50MsCounter % 3 ==0)	
			{
								
				if(RGB_SET_FLAG){
					LIGHT_SET1 = rgb_set(RGB_SET_FLAG,LIGHT_SET1);
				}
				else{
					LIGHT_SET1 = my_2812_breath(LIGHT_SET1);
				}
				MY_2812_FOUR(LIGHT_SET1);
			}   
			//  ###############################
			
			
	
			
			//  ############# OLED #############
			//	0.5S任务
			if(sleepTime50MsCounter %10 ==0 )	
			{
				if(OLED_PIC_LFAG<2){						//	初始化界面
					my_display(RGB_SET_FLAG,0);
					OLED_PIC_LFAG += 1;
					sleepTime1SCounter = 3; 
				}
				else if(RGB_SET_FLAG != BREATH || 
					(RGB_SET_FLAG == BREATH && RGB_SET_TIMEOFF_CNT<5) ){	// 
						my_display(RGB_SET_FLAG,1);		//  RGB配置界面
							
						RGB_SET_TIMEOFF_CNT++;				// 超时标志位递增，在EXTI按键和ENC编码器有输入时清空
						
						if(RGB_SET_TIMEOFF_CNT>=8){		// X*0.5s没有配置动作，切换回待机界面
							RGB_SET_FLAG	=	0;
							RGB_SET_TIMEOFF_CNT = 0;
						}
				}
				else{
						my_display(RGB_SET_FLAG,2);	//	待机界面
				}
			}
			//  ###############################
	

		}
}


