#include "timer1.h"
#include "stm32f10x.h"
#include "stm32f10x_tim.h"
#include "stm32f10x_it.h"
#include <stdio.h>
#include "nvic.h"


#define SVPWM_USE_BDT 	1
#define USE_HARD_PWM 	1
#define PWM_DUTY 80
#define PWM_FRQ 72

#define TimerPeriod  ((72000000 / PWM_FRQ )-1)

/**
  * @brief  Configures the different system clocks.
  * @param  None
  * @retval None
  */
void pwm_rcc_init(void)
{
  /* TIM1, GPIOA, GPIOB, GPIOE and AFIO clocks enable */
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1 | RCC_APB2Periph_GPIOA | 
                         RCC_APB2Periph_AFIO |
                         RCC_APB2Periph_GPIOB, ENABLE);
}

void pwm_cnt_irq_init(void)
{
	
}

/**
  * @brief  Configure the TIM1 Pins.
  * @param  None
  * @retval None
  * @note
  *			PA8 /T1_CH1  ---> HIn3
  *			PA9 /T1_CH2  ---> HIn2
  *			PA10/T1_CH3  ---> HIn1
  *										Out2 ---> PA0/ADC0
  *										Out3 ---> PA1/ADC1
  *			PB15/T1_CHN3 ---> LIn1
  *			PB14/T1_CHN2 ---> LIn2
  *			PB13/T1_CHN1 ---> LIn3
  */
void pwm_pin_init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;

RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB|RCC_APB2Periph_AFIO,ENABLE);

	/* GPIOB Configuration: Channel 1N, 2N and 3N as alternate function push-pull */
	GPIO_InitStructure.GPIO_Pin = GPIO_Pin_13|GPIO_Pin_14 ;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOB, &GPIO_InitStructure);
}

void pwm_tim_init(void){

GPIO_InitTypeDef GPIO_InitStructure;
TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
TIM_OCInitTypeDef TIM_OCInitStructure;

RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
RCC_APB2PeriphClockCmd(RCC_APB2Periph_TIM1,ENABLE);

GPIO_InitStructure.GPIO_Pin=GPIO_Pin_14;
GPIO_InitStructure.GPIO_Speed=GPIO_Speed_50MHz;
GPIO_InitStructure.GPIO_Mode=GPIO_Mode_AF_PP; 
GPIO_Init(GPIOB,&GPIO_InitStructure);

 /* Time Base configuration */
 TIM_TimeBaseInitStructure.TIM_Prescaler = 1000;
 TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
 TIM_TimeBaseInitStructure.TIM_Period = 710;  /// PWM   
 TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
 TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;

 TIM_TimeBaseInit(TIM1, &TIM_TimeBaseInitStructure);

    /* Channel 1, 2,3 and 4 Configuration in PWM mode */
 TIM_OCInitStructure.TIM_OCMode = TIM_OCMode_PWM1;  ///TIM_OCMode_PWM2  TIM_OCMode_Timing
 TIM_OCInitStructure.TIM_OutputState = TIM_OutputState_Disable;
 TIM_OCInitStructure.TIM_OutputNState = TIM_OutputNState_Enable;
 TIM_OCInitStructure.TIM_Pulse = 1000;
 TIM_OCInitStructure.TIM_OCPolarity = TIM_OCPolarity_High;  ///TIM_OCNPolarity_High
 TIM_OCInitStructure.TIM_OCNPolarity = TIM_OCNPolarity_High;
 TIM_OCInitStructure.TIM_OCIdleState = TIM_OCPolarity_High; ///TIM_OCNIdleState_Set
 TIM_OCInitStructure.TIM_OCNIdleState = TIM_OCNPolarity_High;

 TIM_OC2Init(TIM1, &TIM_OCInitStructure);

 /* TIM1 counter enable */
 TIM_Cmd(TIM1, ENABLE);

    /* Main Output Enable */
 TIM_CtrlPWMOutputs(TIM1, ENABLE);


 //TIM_SetCompare1(TIM1, 1000); 
 //TIM_SetCompare2(TIM1, 1000);
}

void pwm_init(void){
	pwm_tim_init();
}


void pwm_disable(void){
	TIM_CtrlPWMOutputs(TIM1, DISABLE);
}

void pwm_enable(void){
	TIM_CtrlPWMOutputs(TIM1, ENABLE);
}


// **** 定时器中断触发 ****  
void TIM1_UP_IRQHandler(void)
{ 

	if(TIM_GetITStatus(TIM1, TIM_IT_Update) != RESET)//  溢出中断
	{

	}	

  TIM_ClearFlag(TIM1, TIM_FLAG_Update); 	
}



