#ifndef __TIMER1_H__
#define __TIMER1_H__
#include "sys.h" 

#define PWM_LED_OFF TIM_SetCompare2(TIM1, 1000);	// LEDϨ��
#define PWM_LED_ON  TIM_SetCompare2(TIM1, 700);	// LEDϨ��

void Timer1Init(u16 arr,u16 psc);
void	pwm_init(void);
void pwm_enable(void);
void pwm_disable(void);

#endif

