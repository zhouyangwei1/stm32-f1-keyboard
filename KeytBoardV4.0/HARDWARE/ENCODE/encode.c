#include "stm32f10x.h"
#include "delay.h"
#include "encode.h"
#include "misc.h"
#include "nvic.h"
#include "sys.h" 
#include "stm32f10x_tim.h"
#include "stm32f10x_exti.h"

void TIM3_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_ICInitTypeDef TIM_ICInitStructure;  	

	//PB6 ch1  ,PB7 ch2 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);//使能TIM4时钟	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);//使能GPIOB时钟
	
	GPIO_StructInit(&GPIO_InitStructure);//将GPIO_InitStruct中的参数按缺省值输入
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_7|GPIO_Pin_6;         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;//PB6 PB7上拉	
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);        


	NVIC_Config(5);

	TIM_DeInit(TIM3);
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = 65535;  //设定计数器重装值   
	TIM_TimeBaseStructure.TIM_Prescaler = 0; //TIM3时钟预分频值,1=1/1000000s
	TIM_TimeBaseStructure.TIM_ClockDivision =TIM_CKD_DIV1 ;//设置时钟分割 T_dts = T_ck_int	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数 
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseStructure);  

	TIM_EncoderInterfaceConfig(TIM3, TIM_EncoderMode_TI12,  TIM_ICPolarity_BothEdge, TIM_ICPolarity_BothEdge);

  TIM_ICStructInit(&TIM_ICInitStructure);
  TIM_ICInitStructure.TIM_ICFilter = 6;//ICx_FILTER;
  TIM_ICInit(TIM3, &TIM_ICInitStructure);
	
	TIM_ARRPreloadConfig(TIM3, ENABLE); //使能TIMx在ARR上的预装载寄存器
                 

 // Clear all pending interrupts
    TIM_ClearFlag(TIM3, TIM_FLAG_Update);
    TIM_ITConfig(TIM3, TIM_IT_Update, DISABLE);
		
   //Reset counter
	TIM3->CNT = 0;
	TIM_Cmd(TIM3, ENABLE);  

}

void TIM2_Init(void)
{
	GPIO_InitTypeDef GPIO_InitStructure;
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
  TIM_ICInitTypeDef TIM_ICInitStructure;  	

	//PB6 ch1  ,PB7 ch2 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);//使能TIM2时钟	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);//使能GPIOB时钟
	
	//GPIO_StructInit(&GPIO_InitStructure);//将GPIO_InitStruct中的参数按缺省值输入
	GPIO_InitStructure.GPIO_Pin =  GPIO_Pin_0|GPIO_Pin_1;         
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING ;//PA0 PA1
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);        


	TIM_DeInit(TIM2);
	TIM_TimeBaseStructInit(&TIM_TimeBaseStructure);
	TIM_TimeBaseStructure.TIM_Period = 65535;  //设定计数器重装值   
	TIM_TimeBaseStructure.TIM_Prescaler = 0; //TIM3时钟预分频值,1=1/1000000s
	TIM_TimeBaseStructure.TIM_ClockDivision =TIM_CKD_DIV1 ;//设置时钟分割 T_dts = T_ck_int	
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up; //TIM向上计数 
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);  

	TIM_EncoderInterfaceConfig(TIM2, TIM_EncoderMode_TI12,  TIM_ICPolarity_BothEdge, TIM_ICPolarity_BothEdge);

  TIM_ICStructInit(&TIM_ICInitStructure);
  TIM_ICInitStructure.TIM_ICFilter = 6;//ICx_FILTER;
  TIM_ICInit(TIM2, &TIM_ICInitStructure);
	
	TIM_ARRPreloadConfig(TIM2, ENABLE); //使能TIMx在ARR上的预装载寄存器
                 

 // Clear all pending interrupts
  TIM_ClearFlag(TIM2, TIM_FLAG_Update);
  TIM_ITConfig(TIM2, TIM_IT_Update, DISABLE);
	// NVIC_Config(5);
		
   //Reset counter
	TIM2->CNT = 0;
	TIM_Cmd(TIM2, ENABLE);  

}


int get_enc_cnt_inc(char TIMx){
	
	int ins_val;
	switch (TIMx)
	{
		case 3: ins_val = (short int)(TIM3->CNT);	  TIM3->CNT = 0;  break;
		case 2: ins_val = (short int)(TIM2->CNT);		TIM2->CNT = 0;  break;
		default: ins_val = 0;
	}
	//DelayUs(10);

	return	ins_val;
	
}

