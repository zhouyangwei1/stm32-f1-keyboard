#include "exti.h"
#include "nvic.h"
#include "led.h"
#include "sys.h" 
#include "delay.h"
#include "timer.h"
#include "stm32f10x_exti.h"
#include "stm32f10x_tim.h"
#include "main.h"

#define GPIO_PortSourceGPIOx   GPIO_PortSourceGPIOA
#define GPIO_PinSourcex_0      GPIO_PinSource2
#define GPIO_PinSourcex_1      GPIO_PinSource3
#define GPIO_PinSourcex_2      GPIO_PinSource4
#define EXTI_Linex_0           EXTI_Line2
#define EXTI_Linex_1           EXTI_Line3
#define EXTI_Linex_2           EXTI_Line4
#define PeriphClock_AFIO       RCC_APB2Periph_AFIO
#define PeriphClock_GPIO       RCC_APB2Periph_GPIOA
#define EXTI_Pin0              GPIO_Pin_2
#define EXTI_Pin1              GPIO_Pin_3 
#define EXTI_Pin2              GPIO_Pin_4  
#define EXTI_Pin_GPIO_Group    GPIOA


void EXTI_KEY_Config(void)
{
	GPIO_InitTypeDef GPIO_InitStructure; 
	EXTI_InitTypeDef EXTI_InitStructure;

	/* config the extiline clock and AFIO clock */
	RCC_APB2PeriphClockCmd(PeriphClock_GPIO| PeriphClock_AFIO,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	/* config the NVIC */
	

	NVIC_Config(1);
		

	/* EXTI line gpio config*/	
	GPIO_InitStructure.GPIO_Pin = EXTI_Pin1|EXTI_Pin2;       
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	 // 浮空輸入
	GPIO_Init(EXTI_Pin_GPIO_Group, &GPIO_InitStructure);
	
//		/* EXTI line gpio config*/	
//	GPIO_InitStructure.GPIO_Pin = EXTI_Pin0;       
//	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN_FLOATING;	 // 浮空輸入
//	GPIO_Init(GPIOA, &GPIO_InitStructure);

//	// EXTI line mode config 
//	GPIO_EXTILineConfig(GPIO_PortSourceGPIOA,GPIO_PinSourcex_0); 
//	EXTI_InitStructure.EXTI_Line = EXTI_Linex_0;
//	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; // 下降沿
//	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//	EXTI_Init(&EXTI_InitStructure); 
  
	/* EXTI line mode config */
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOx,GPIO_PinSourcex_1); 
	EXTI_InitStructure.EXTI_Line = EXTI_Linex_1;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; //下降沿
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure); 
	
		/* EXTI line mode config */
	GPIO_EXTILineConfig(GPIO_PortSourceGPIOx,GPIO_PinSourcex_2); 
	EXTI_InitStructure.EXTI_Line = EXTI_Linex_2;
	EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Falling; // 
	EXTI_InitStructure.EXTI_LineCmd = ENABLE;
	EXTI_Init(&EXTI_InitStructure); 
	

}

void dual_press_process(void){
	
	sleepTime1SCounter=9999;	//熄灭

}
void long_press_process(char EXTIx){
				
	if(EXTIx==3){
		RGB_SET_FLAG = BREATH;
	}
	
	
	else if(EXTIx==4){
		// #####################  熄屏  ####################
//		g_myKeyBoard_DataWaitForUploadFlag	=	0;
//		sleepTime1SCounter=9999;	//熄灭
		// #################################################
		BTK05_ATKeyDataPack[6]	=	97;
		BTK05_ATKeyDataPack[7]	=	95;
		BTK05_ATKeyDataPack[8]	=	98;
		BTK05_ATKeyDataPack[9]	=	96;
		BTK05_ATKeyDataPack[10]	=	89;
		BTK05_ATKeyDataPack[11]	=	93;
		Keyboard_Send(BTK05_ATKeyDataPack+4);			// ####发送####	970815
		DelayMs(100);
		BTK05_ATKeyDataPack[6]	=	0;
		BTK05_ATKeyDataPack[7]	=	0;
		BTK05_ATKeyDataPack[8]	=	0;
		BTK05_ATKeyDataPack[9]	=	0;
		BTK05_ATKeyDataPack[10]	=	0;
		BTK05_ATKeyDataPack[11]	=	0;
		Keyboard_Send(BTK05_ATKeyDataPack+4);			// ####发送####	
		
	}
}

u8	l_press_cntr	= 0;
void EXTI3_IRQHandler(void)
{
  EXTI_ClearITPendingBit(EXTI_Line3);	
	
	DelayMs(20); 
	if(PAin(3)==0)
	{  
							
		while(PAin(3)==0)	{
				DelayMs(10);
				l_press_cntr++;
				//##########  双按  ##########
				if(PAin(4)==0){
					dual_press_process();
					l_press_cntr = 0;
					return;
				}
				//##########  长按  ##########	
				else if(l_press_cntr>80)	{
					
					long_press_process(3);
					l_press_cntr = 0;
					return;
				}

		}	
		
		//########## 单点  ##########
		//  ##########  唤醒  #########
		g_myKeyBoard_DataWaitForUploadFlag	=	1;
		sleepTime1SCounter = 0;		// 清空计数
		//  ##########################
		//  ##########################
		RGB_SET_FLAG	+=	1;			// RGB设定目标切换
		RGB_SET_TIMEOFF_CNT = 0;	// 清空配置超时标志位
		if(RGB_SET_FLAG>4)	RGB_SET_FLAG =	1;	// 溢出清除
		//  ##########################
		
		l_press_cntr = 0;

	}				
}


// 短按静音，长按息屏
volatile u8 VOL_PAUS_FLAG = 0;
void EXTI4_IRQHandler(void)
{
	EXTI_ClearITPendingBit(EXTI_Line4);
	
	DelayMs(20);  // 软件防抖
	if(PAin(4)==0)
	{
		
		while(PAin(4)==0)	{
				DelayMs(10);
				l_press_cntr++;
			//##########  双按  ##########
				if(PAin(3)==0){
					dual_press_process();
					l_press_cntr = 0;
					return;
				}
			//##########  长按  ##########
				else if(l_press_cntr>80)	{
					
					long_press_process(4);
					l_press_cntr = 0;
					return;
				}
		}		
		
		//##########  单按  ##########
		//  ##########  唤醒  #########
		g_myKeyBoard_DataWaitForUploadFlag	=	1;
		sleepTime1SCounter = 0;		// 清空计数
		//  ##########################
		VOL_PAUS_FLAG	=	!VOL_PAUS_FLAG;
		
		l_press_cntr = 0;		
		
		

	}				
}


