#ifndef __BSP_LEDSTRIP_H
#define	__BSP_LEDSTRIP_H
#include  "sys.h"
#include "delay.h"

#define BREATH	0
#define MAX			1
#define RED			2
#define GREEN		3
#define	BLUE		4



struct	LIGHT_SET{
	uint8_t green;
	uint8_t blue;
	uint8_t red;
} extern LIGHT_SET1 ;



void RGB_LED_Write_24Bits(uint8_t green,uint8_t red,uint8_t blue);
void MY_2812_FOUR(struct LIGHT_SET light_set);
struct LIGHT_SET my_2812_breath(struct LIGHT_SET light_set);
struct LIGHT_SET	my_2812_set(char col,	uint8_t val, struct LIGHT_SET light_set);
struct LIGHT_SET	my_2812_set_dif(char col,	uint8_t val, struct LIGHT_SET light_set);

void RGB_LED_Init(void);
void RGB_LED_Red(void);
void RGB_LED_Green(void);
void RGB_LED_Blue(void);
void RGB_LED_Reset(void);
#endif /* __BSP_LEDSTRIP_H */

