/**
  ******************************************************************************
  * @file    usb_endp.c
  * @author  MCD Application Team
  * @version V4.0.0
  * @date    21-January-2013
  * @brief   Endpoint routines
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_istr.h"
#include "myKeyBoard.h"
/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
extern __IO uint8_t PrevXferComplete;

/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
/*******************************************************************************
* Function Name  : EP1_OUT_Callback.
* Description    : EP1 OUT Callback Routine.
* Input          : None.
* Output         : None.
* Return         : None.
*******************************************************************************/
//void EP1_IN_Callback(void)
//{
//  /* Set the transfer complete token to inform upper layer that the current 
//  transfer has been complete */
//  PrevXferComplete = 1; 
//}
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
 
 
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/
 
/**
 * @brief  EP1 OUT Callback Routine.
 */
void EP1_OUT_Callback(void)
{
  /* Enable the receive of data on EP1 */
  SetEPRxStatus(ENDP1, EP_RX_VALID);
}
 
/**
 * @brief  EP1 IN Callback Routine.
 */
void EP1_IN_Callback(void)
{
 
}
 
void EP2_OUT_Callback(void)
{	
//	USER_USB_HIDEP2_RX_LEN = USB_GetEpRxCnt(ENDP2);
//	USB_CopyPMAToUserBuf((unsigned char*)USER_USB_HIDEP2_RX_Buf, ENDP2_RXADDR, USER_USB_HIDEP2_RX_LEN);	
// 
//	HtClient_RcvBufDataIn(MODE_HID2, USER_USB_HIDEP2_RX_Buf, USER_USB_HIDEP2_RX_LEN);
	
	/* Enable the receive of data on EP2 */
	SetEPRxStatus(ENDP2, EP_RX_VALID);
}
 
void EP2_IN_Callback(void)
{
 
 
}

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

