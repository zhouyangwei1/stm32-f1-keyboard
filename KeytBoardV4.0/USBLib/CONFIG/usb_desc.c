/**
  ******************************************************************************
  * @file    usb_desc.c
  * @author  MCD Application Team
  * @version V4.0.0
  * @date    21-January-2013
  * @brief   Descriptors for Joystick Mouse Demo
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT 2013 STMicroelectronics</center></h2>
  *
  * Licensed under MCD-ST Liberty SW License Agreement V2, (the "License");
  * You may not use this file except in compliance with the License.
  * You may obtain a copy of the License at:
  *
  *        http://www.st.com/software_license_agreement_liberty_v2
  *
  * Unless required by applicable law or agreed to in writing, software 
  * distributed under the License is distributed on an "AS IS" BASIS, 
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  ******************************************************************************
  */


/* Includes ------------------------------------------------------------------*/
#include "usb_lib.h"
#include "usb_desc.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/
/* Extern variables ----------------------------------------------------------*/
/* Private function prototypes -----------------------------------------------*/
/* Private functions ---------------------------------------------------------*/

#define		USB_VID		0x5548
#define		USB_PID		0x6666
#define		DEV_VER		0x0110

const uint8_t Joystick_DeviceDescriptor[JOYSTICK_SIZ_DEVICE_DESC] =
  {
    0x12,                       /*bLength */
		USB_DEVICE_DESCRIPTOR_TYPE, /*bDescriptorType*/
		0x00,                       /*bcdUSB */
		0x02,
		0x00,                       /*bDeviceClass*/
		0x00,                       /*bDeviceSubClass*/
		0x00,                       /*bDeviceProtocol*/
		0x40,                       /*bMaxPacketSize40*/
		USB_VID&0xff,					/*idVendor*/
		USB_VID>>8, 					
		USB_PID&0xff,					/*idProduct*/
		USB_PID>>8,
		DEV_VER&0xff,					/*bcdDevice rel. 1.00*/
		DEV_VER>>8,
		1,                          /*Index of string descriptor describing
																							manufacturer */
		2,                          /*Index of string descriptor describing
																						 product*/
		3,                          /*Index of string descriptor describing the
																						 device serial number */
		0x01                        /*bNumConfigurations*/
}; /* keyboard设备描述符 */



/* USB配置描述符集合(配置、接口、端点、类、厂商)(Configuration, Interface, Endpoint, Class, Vendor */
const uint8_t Joystick_ConfigDescriptor[JOYSTICK_SIZ_CONFIG_DESC] =
{
	
		0x09, /* bLength: Configuration Descriptor size */
		USB_CONFIGURATION_DESCRIPTOR_TYPE, /* bDescriptorType: Configuration */
		JOYSTICK_SIZ_CONFIG_DESC,
		/* wTotalLength: Bytes returned */
		0x00,
		0x02,         /* bNumInterfaces: 1 interface */
		0x01,         /* bConfigurationValue: Configuration value */
		0x00,         /* iConfiguration: Index of string descriptor describing
																 the configuration*/
		0xC0,         /* bmAttributes: Self powered */
		0x96,         /* MaxPower 100 mA: this current is used for detecting Vbus */
 
		/************** Descriptor of Custom HID interface ****************/
		/* 09 */
		0x09,         /* bLength: Interface Descriptor size */
		USB_INTERFACE_DESCRIPTOR_TYPE,/* bDescriptorType: Interface descriptor type */
		0x00,         /* bInterfaceNumber: Number of Interface */
		0x00,         /* bAlternateSetting: Alternate setting */
		0x02,         /* bNumEndpoints */
		0x03,         /* bInterfaceClass: HID */
		0x01,         /* bInterfaceSubClass : 1=BOOT, 0=no boot */
		0x01,         /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */
		0,            /* iInterface: Index of string descriptor */
		/******************** Descriptor of Custom HID HID ********************/
		/* 18 */
		0x09,         /* bLength: HID Descriptor size */
		HID_DESCRIPTOR_TYPE, /* bDescriptorType: HID */
		0x10,         /* bcdHID: HID Class Spec release number */
		0x01,
		0x00,         /* bCountryCode: Hardware target country */
		0x01,         /* bNumDescriptors: Number of HID class descriptors to follow */
		0x22,         /* bDescriptorType */
		sizeof(Joystick_ReportDescriptor)&0xFF,		  /* wItemLength: Total length of Report descriptor :36 bytes*/
		(sizeof(Joystick_ReportDescriptor)>>8)&0xFF,
		/******************** Descriptor of Custom HID endpoints ******************/
		/* 27 */
		0x07,          /* bLength: Endpoint Descriptor size */
		USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType: */
 
		0x81,          /* bEndpointAddress: Endpoint Address (IN) */
		0x03,          /* bmAttributes: Interrupt endpoint */
		0x08,          /* wMaxPacketSize: 64 Bytes max */
		0x00,
		0x0A,          /* bInterval: Polling Interval (32 ms) */
		/* 34 */
			
		0x07,	/* bLength: Endpoint Descriptor size */
		USB_ENDPOINT_DESCRIPTOR_TYPE,	/* bDescriptorType: */
			/*	Endpoint descriptor type */
		0x01,	/* bEndpointAddress: */
			/*	Endpoint Address (OUT) */
		0x03,	/* bmAttributes: Interrupt endpoint */
		0x08,	/* wMaxPacketSize: 64 Bytes max  */
		0x00,
		0x0A,		   /* bInterval: Polling Interval (10 ms) */ 
 
//下面是自己添加的hid设备相关描述//
	/************** Descriptor of 2/0 HID interface ****************/
		/* 41 */
		0x09,		  /* bLength: Interface Descriptor size */
		USB_INTERFACE_DESCRIPTOR_TYPE,/* bDescriptorType: Interface descriptor type */
		0x01,		  /* bInterfaceNumber: Number of Interface这里改成1 */
		0x00,		  /* bAlternateSetting: Alternate setting */
		0x02,		  /* bNumEndpoints这里改成2 */
		0x03,		  /* bInterfaceClass: HID */
		0x00,		  /* bInterfaceSubClass : 1=BOOT, 0=no boot */
		0x00,		  /* nInterfaceProtocol : 0=none, 1=keyboard, 2=mouse */
		0x00,		  /* iInterface: Index of string descriptor */
		/******************** Descriptor of 2/0 HID ********************/
		/* 50 */
		0x09,		  /* bLength: HID Descriptor size */
		HID_DESCRIPTOR_TYPE, /* bDescriptorType: HID */
		0x10,		  /*bcdHID: HID Class Spec release number*/
		0x01,
		0x00,		  /* bCountryCode: Hardware target country */
		0x01,		  /* bNumDescriptors: Number of HID class descriptors to follow */
		0x22,		  /* bDescriptorType */
		sizeof(HID_driver_ReportDescriptor)&0xFF,		  /* wItemLength: Total length of Report descriptor :36 bytes*/
		(sizeof(HID_driver_ReportDescriptor)>>8)&0xFF,
		/******************** Descriptor of 2/0 HID endpoints :3 In, Interrupt, 3 ms******************/
		/* 59 */
		0x07,		   /* bLength: Endpoint Descriptor size */
		USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType: */
		0x82,		   /* bEndpointAddress: Endpoint Address (IN) */
		0x03,		   /* bmAttributes: Interrupt endpoint */
		0x40,		   /* wMaxPacketSize: 10 Bytes max */
		0x00,
		0x22,		   /* bInterval: Polling Interval (10 ms) */ 
		/******************** Descriptor of 2/0 HID endpoints :3 Out, Interrupt, 3 ms******************/
		/* 66 */
		0x07,		   /* bLength: Endpoint Descriptor size */
		USB_ENDPOINT_DESCRIPTOR_TYPE, /* bDescriptorType: */
		0x02,		   /* bEndpointAddress: Endpoint Address (out) */
		0x03,		   /* bmAttributes: Interrupt endpoint */
		0x40,		   /* wMaxPacketSize: 10 Bytes max */
		0x00,
		0x0A,		   /* bInterval: Polling Interval (10 ms) */ 
		/* 73 */
///添加结束//

}; 



/* HID的报告描述符*/
/*定义了8字节发送：
**  第一字节表示特殊件是否按下：D0:Ctrl D1:Shift D2:Alt
**  第二字节保留，值为0
**  第三~第八字节:普通键键值的数组,最多能同时按下6个键
**定义了1字节接收:对应键盘上的LED灯,这里只用了两个位。
**	D0:Num Lock   D1:Cap Lock   D2:Scroll Lock   D3:Compose   D4:Kana*/
const uint8_t Joystick_ReportDescriptor[JOYSTICK_SIZ_REPORT_DESC] = 
{ 	
    0x05, 0x01,                    // USAGE_PAGE (Generic Desktop)
    0x09, 0x06,                    // USAGE (Keyboard)
    0xa1, 0x01,                    // COLLECTION (Application)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0xe0,                    //   USAGE_MINIMUM (Keyboard LeftControl)
    0x29, 0xe7,                    //   USAGE_MAXIMUM (Keyboard Right GUI)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x25, 0x01,                    //   LOGICAL_MAXIMUM (1)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x95, 0x08,                    //   REPORT_COUNT (8)
    0x81, 0x02,                    //   INPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x81, 0x03,                    //   INPUT (Cnst,Var,Abs)
    0x95, 0x05,                    //   REPORT_COUNT (5)
    0x75, 0x01,                    //   REPORT_SIZE (1)
    0x05, 0x08,                    //   USAGE_PAGE (LEDs)
    0x19, 0x01,                    //   USAGE_MINIMUM (Num Lock)
    0x29, 0x05,                    //   USAGE_MAXIMUM (Kana)
    0x91, 0x02,                    //   OUTPUT (Data,Var,Abs)
    0x95, 0x01,                    //   REPORT_COUNT (1)
    0x75, 0x03,                    //   REPORT_SIZE (3)
    0x91, 0x03,                    //   OUTPUT (Cnst,Var,Abs)
    0x95, 0x06,                    //   REPORT_COUNT (6)
    0x75, 0x08,                    //   REPORT_SIZE (8)
    0x15, 0x00,                    //   LOGICAL_MINIMUM (0)
    0x26, 0xff, 0x00,              //   LOGICAL_MAXIMUM (255)
    0x05, 0x07,                    //   USAGE_PAGE (Keyboard)
    0x19, 0x00,                    //   USAGE_MINIMUM (Reserved (no event indicated))
    0x29, 0x81,                    //   USAGE_MAXIMUM (Keyboard Application)
    0x81, 0x00,                    //   INPUT (Data,Ary,Abs)
    0xc0                           // END_COLLECTION
}; 

const uint8_t HID_driver_ReportDescriptor[HID_driver_SIZ_REPORT_DESC] =
{         
//	//vendor
//	0x06, 0x00, 0xFF,  	//Usage Page (Vendor-Defined 29) 
//	0x09, 0x92,  				//Usage (Vendor-Defined 146) 
//	0xA1, 0x01,					//Collection (Application)   
//    
//	0x19, 0x00,  				//Usage Minimum (Undefined) 
//  0x2A, 0xFF, 0x00,		//Usage Maximum (Vendor-Defined 255)   
//  0x15, 0x00,					//Logical Minimum (0)   
//  0x26, 0xFF, 0x00,		//Logical Maximum (255)   
//  0x75, 0x08,					//Report Size (8)   
//  0x95, 0x40, 				//Report Count (63)  
//  0x91, 0x00, 				//Output (Data,Ary,Abs,NWrp,Lin,Pref,NNul,NVol,Bit)  
//    
//	0x19, 0x00,  				//Usage Minimum (Undefined) 
//  0x2a, 0xFF, 0x00,		//Usage Maximum (Vendor-Defined 255)   
//    
//	0x81, 0x00,  				//Input (Data,Ary,Abs) 
//	0xC0, 							//End Collection 
	
		0x05, 0x0c,  
		0x09, 0x01,
		0xa1, 0x01,  
		0xa1, 0x00, 
		0x09, 0xe9, 
		0x09, 0xea, 
		0x09, 0xe2,   
		0x09, 0xcd,
		0x35, 0x00,  
		0x45, 0x07, 
		0x15, 0x00,
		0x25, 0x01,
		0x75, 0x01,
		0x95, 0x04, 
		0x81, 0x02, 
		0x75, 0x01,
		0x95, 0x04, 
		0x81, 0x01, 
		0xc0, 
		0xc0


}; 


/* USB String Descriptors (optional) */
const uint8_t Joystick_StringLangID[JOYSTICK_SIZ_STRING_LANGID] =
  {
    JOYSTICK_SIZ_STRING_LANGID,
    USB_STRING_DESCRIPTOR_TYPE,
    0x09,
    0x04
  }
  ; /* LangID = 0x0409: U.S. English */

const uint8_t Joystick_StringVendor[JOYSTICK_SIZ_STRING_VENDOR] =
  {
    JOYSTICK_SIZ_STRING_VENDOR, /* Size of Vendor string */
    USB_STRING_DESCRIPTOR_TYPE,  /* bDescriptorType*/
    /* Manufacturer: "STMicroelectronics" */
    'S', 0, 'T', 0, 'M', 0, 'i', 0, 'c', 0, 'r', 0, 'o', 0, 'e', 0,
    'l', 0, 'e', 0, 'c', 0, 't', 0, 'r', 0, 'o', 0, 'n', 0, 'i', 0,
    'c', 0, 's', 0
  };

const uint8_t Joystick_StringProduct[JOYSTICK_SIZ_STRING_PRODUCT] =
{
	JOYSTICK_SIZ_STRING_PRODUCT,          /* bLength */
	USB_STRING_DESCRIPTOR_TYPE,        /* bDescriptorType */
	'L', 0, 'X', 0, 'G', 0, '-', 0, 'W', 0, 'O', 0, 'R', 0, 'L', 0, 'D', 0, ' ', 0, 'E', 0, 'D', 0, 'I', 0, 'T', 0,
	' ', 0, 
	0X1B,0X52,//
	0x16,0X4E,//
	0X00,0X4E,//鼠
	0XF7,0X53,//标 
};

uint8_t Joystick_StringSerial[JOYSTICK_SIZ_STRING_SERIAL] =
  {
    JOYSTICK_SIZ_STRING_SERIAL,           /* bLength */
    USB_STRING_DESCRIPTOR_TYPE,        /* bDescriptorType */
    'S', 0, 'T', 0, 'M', 0, '3', 0, '2', 0
  };

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

