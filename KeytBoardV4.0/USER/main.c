#include "main.h"


#define wake_up_macro		0
#define sleep_macro			1


void TIM2_Int_Init(u16 arr,u16 psc);
void TIM3_CH3_PWM_init(u16 arr,u16 psc);
void myUSB_Init(void);
void sysInit(void);
void wake_up(char state);
unsigned char system_Status = 1;  //1:运行中，0：休眠中
unsigned char g_Flag = 0;
uint8_t ledLogoPWM = 80;
int8_t ledLogoPWMDelt = 0;
bool g_USBModeFlag = TRUE;
struct LIGHT_SET LIGHT_SLEEP = {0x00,0x00,0x00};
unsigned char VLOUME_BUF[5]={0};

int main()
{
	sysInit();

	while(1)
	{ 
		// #####################	主任务  ####################
		if(g_USBModeFlag)//USB模式下
		{
			//		###############  运行状态  #############
			if(sleepTime1SCounter <= 600){
				myKeyBoard_ScanKeyAndUpdataATBuffer();				//键盘事件更新
				
				if(g_myKeyBoard_DataWaitForUploadFlag == 1){	// 如果检测到更新
					sleepTime1SCounter = 0;											// 清空休眠计数
					g_myKeyBoard_DataWaitForUploadFlag = 0;
					if(bDeviceState == CONFIGURED){							//如果连接上了 USB
						Keyboard_Send(BTK05_ATKeyDataPack+4);			// ####发送####
					}
				}
			}
			
			
			//		###############  休眠状态  #############
			else if(system_Status == 0){
				myKeyBoard_ScanKeyAndUpdataATBuffer();				// 键盘事件更新
				
				if(g_myKeyBoard_DataWaitForUploadFlag == 1){	// 如果检测到更新
					g_myKeyBoard_DataWaitForUploadFlag = 0;
					
					if(bDeviceState == CONFIGURED){							// 如果连接上了 USB
							Keyboard_Send(BTK05_ATKeyDataPack+4);		// ####发送####
						}
						
						wake_up(wake_up_macro);
				}
			}
			
		  //		###############  进入休眠  #############
			else if(sleepTime1SCounter > 1000)
			{
				wake_up(sleep_macro);											// 熄灭OLED
			}
		}

		
			
	}

}

// ##############   ############
void wake_up(char state){
	switch(state){
		case wake_up_macro: 	
			OLED_PIC_LFAG	= 0;													// 开机界面
			system_Status = 1;													// 唤醒标志位
			RGB_SET_FLAG = BREATH;
			PWM_LED_OFF;																// 小灯熄灭
			sleepTime1SCounter = 0;											// 清空休眠计数
					
//			TIM_Cmd(TIM4, ENABLE);  										// 使能TIMx	
			TIM4->CNT = 990;														// 直接触发TIM中断，加快响应
		break;
		
		case sleep_macro:
//			TIM_Cmd(TIM4, DISABLE);  											// 失能TIMx				
		
			PWM_LED_ON;																		// LED点亮
			system_Status = 0;														// 复位系统标志位
			MY_2812_FOUR(LIGHT_SLEEP);										// 熄灭RGB
			OLED_Clear();		
			
		break;
	}
}

void sysInit()
{

	GPIO_DeInit(GPIOA);
	GPIO_DeInit(GPIOB);
	GPIO_DeInit(GPIOC);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOB,ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOC,ENABLE);
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);
	DelayInit();
	
	OLED_Init(0,0); 
	my_display(RGB_SET_FLAG,0);
	
	myKeyBoard_GPIO_Init();
	
	//LED_LOGO_GPIO_Init();
	EXTI_KEY_Config();
	
	RGB_LED_Init();
	
	MY_2812_FOUR(LIGHT_SET1);
 
	
	TIM3_Init();
	TIM2_Init();
	TIM4_Init(1000,3600);//50ms一次中断
	pwm_init();
	
	myUSB_Init();
	
	//TIM2->CNT = 990;														// 直接触发TIM中断，加快响应
}



//通用定时器2中断初始化
//这里时钟选择为APB1的2倍，而APB1为36M
//arr：自动重装值。
//psc：时钟预分频数
//一次中断的时间为t：t = (arr * psc / APB1*2) * 1000 ms
void TIM2_Int_Init(u16 arr,u16 psc)
{
   TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
 
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE); //时钟使能
	
	//定时器TIM3初始化
	TIM_TimeBaseStructure.TIM_Period = arr; //设置在下一个更新事件装入活动的自动重装载寄存器周期的值	
	TIM_TimeBaseStructure.TIM_Prescaler =psc; //设置用来作为TIMx时钟频率除数的预分频值
	TIM_TimeBaseStructure.TIM_ClockDivision = TIM_CKD_DIV1; //设置时钟分割:TDTS = Tck_tim
	TIM_TimeBaseStructure.TIM_CounterMode = TIM_CounterMode_Up;  //TIM向上计数模式
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure); //根据指定的参数初始化TIMx的时间基数单位
 
	TIM_ITConfig(TIM2,TIM_IT_Update,ENABLE ); //使能指定的TIM中断,允许更新中断
 
	//中断优先级NVIC设置
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;  //TIM中断
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;  //先占优先级级
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;  //从优先级级
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE; //IRQ通道被使能
	NVIC_Init(&NVIC_InitStructure);  //初始化NVIC寄存器

	TIM_Cmd(TIM2, ENABLE);  //使能TIMx					 
}


void USB_Mode(void)
{

	g_USBModeFlag = TRUE;	
	
	sleepTime1SCounter = 0;
	TIM_Cmd(TIM4, ENABLE);  		
	//system_Status = 1;
	//LED_GoToSleep();
	//GPIO_SetBits(LED_LOGO_GPIOPort,LED_LOGO_GPIOPin);
}

void BLUETEETH_Mode(void)
{
 
	g_USBModeFlag = FALSE;	
	
	BTK05_Wake();//唤醒BTK05
	TIM_Cmd(TIM4, ENABLE);  //使能TIMx	
	sleepTime1SCounter = 0;
	//GPIO_ResetBits(LED_LOGO_GPIOPort,LED_LOGO_GPIOPin);
}


void myUSB_Init(void)
{
	USB_Port_Set(1);	//开启USB连接
	//USB配置
 	USB_Interrupts_Config();    
 	Set_USBClock();   
 	USB_Init();	 
}



//void LED_LOGO_FLASH_3TIMES()
//{
//	
////------------------------------------------------------
//	

//}


