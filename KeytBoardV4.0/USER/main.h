#ifndef __MAIN_H_
#define __MAIN_H_

#include <stm32f10x.h> 
#include "stm32f10x_it.h"
#include "LED.h"
#include "oled.h"
#include "timer.h"
#include "delay.h"
#include "myKeyBoard.h"
#include "BTK05.h"
#include "hw_config.h"
#include "usb_lib.h"
#include "usb_pwr.h"
#include "timer1.h"
#include "encode.h"
#include "ws2812.h"
#include "exti.h"
#include "timer.h"
#include "nvic.h"

//typedef enum
//{
//  FALSE = 0, TRUE  = !FALSE
//}
//bool;

#define true 1
#define false 0
	
extern unsigned char g_Flag;
extern bool g_USBModeFlag;
extern unsigned char LED_LOGOENBreath_Flag ;
extern char Comsumer_Mode;
extern unsigned char light_set[4];
extern unsigned short sleepTime1SCounter;
extern unsigned char sleepTime50MsCounter;
extern unsigned char system_Status;
extern unsigned char RGB_SET_TIMEOFF_CNT;
volatile extern u8 RGB_SET_FLAG, MAX_LIT, OLED_PIC_LFAG,VOL_PAUS_FLAG ;


//void LED_LOGO_FLASH_3TIMES(void);
void BLUETEETH_Mode(void);
void USB_Mode(void);





#endif



